FROM liliancal/ubuntu-php-apache
RUN apt-get update \
   && apt-get -y upgrade
ADD src /var/www/
EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
